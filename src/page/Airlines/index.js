import React, { Component } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { addAirline } from "../../store/action";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { Button } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
	root: {
		"& > *": {
			margin: theme.spacing(1),
			width: "25ch",
		},
	},
}));

class Airline extends Component {
	constructor(props) {
		super(props);
		this.state = {
			airlineDetails: {
				name: "",
				code: "",
			},
		};
	}

	onChangeHandler = (event) => {
		let value = event.target.value;
		let name = event.target.name;
		this.setState({
			airlineDetails: {
				...this.state.airlineDetails,
				[name]: value,
			},
		});
	};

	onAirlineSubmit = () => {
		this.props.dispatch(addAirline(this.state.airlineDetails));
		this.setState({
			airlineDetails: {
				name: "",
				code: "",
			},
		});
	};
	render() {
		const { classes } = this.props;
		return (
			<div>
				<h1>Add Airline</h1>

				<div>
					<TextField
						id="standard-basic"
						label="name"
						type="text"
						name="name"
						variant="outlined"
						value={this.state.airlineDetails.name}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<TextField
						id="filled-basic"
						label="code"
						variant="filled"
						name="code"
						value={this.state.airlineDetails.code}
						variant="outlined"
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<Button
						variant="contained"
						color="primary"
						onClick={this.onAirlineSubmit}
					>
						submit
					</Button>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	console.log(state);
};

export default withStyles(useStyles)(
	withRouter(connect(mapStateToProps)(withRouter(Airline)))
);
