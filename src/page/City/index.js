import React, { Component } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { addCity } from "../../store/action";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";

import { Button } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
	root: {
		"& > *": {
			margin: theme.spacing(1),
			width: "25ch",
		},
	},
}));

class City extends Component {
	constructor(props) {
		super(props);
		this.state = {
			cityDetails: {
				name: "",
				code: "",
			},
		};
	}

	onChangeHandler = (event) => {
		let value = event.target.value;
		let name = event.target.name;
		this.setState({
			cityDetails: {
				...this.state.cityDetails,
				[name]: value,
			},
		});
	};

	onCitySubmit = () => {
		this.props.dispatch(addCity(this.state.cityDetails));
		this.setState({
			cityDetails: {
				name: "",
				code: "",
			},
		});
	};
	render() {
		const { classes } = this.props;
		return (
			<div>
				<h1>Add City</h1>

				<div>
					<TextField
						id="standard-basic"
						label="name"
						type="text"
						name="name"
						variant="outlined"
						value={this.state.cityDetails.name}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<TextField
						id="filled-basic"
						label="code"
						variant="filled"
						name="code"
						value={this.state.cityDetails.code}
						variant="outlined"
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<Button
						variant="contained"
						color="primary"
						onClick={this.onCitySubmit}
					>
						submit
					</Button>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	console.log(state);
};

export default withStyles(useStyles)(
	withRouter(connect(mapStateToProps)(withRouter(City)))
);
