import React, { Component } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import { addAirline, addFlight } from "../../store/action";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import { MenuItem, InputLabel, Select, FormControl } from "@material-ui/core";
import { Button } from "@material-ui/core";
const useStyles = makeStyles((theme) => ({
	root: {
		"& > *": {
			margin: theme.spacing(1),
			width: "25ch",
		},
	},
	formControl: {
		margin: theme.spacing(1),
		minWidth: 120,
		maxWidth: 300,
	},
	selectEmpty: {
		marginTop: theme.spacing(2),
	},
}));

class Flights extends Component {
	constructor(props) {
		super(props);
		this.state = {
			flightDetails: {
				seats: "",
				flightNumber: "",
				departureTime: "",
				arrivalTime: "",
				from: "",
				to: "",
				airline: "",
				cost: "",
				quantity: 0,
			},
		};
	}

	onChangeHandler = (event) => {
		let value = event.target.value;
		let name = event.target.name;
		this.setState({
			flightDetails: {
				...this.state.flightDetails,
				[name]: value,
			},
		});
	};

	onFlightSubmit = () => {
		this.props.dispatch(addFlight(this.state.flightDetails));
		this.setState({
			flightDetails: {
				seats: "",
				flightNumber: "",
				departureTime: "",
				arrivalTime: "",
				from: "",
				to: "",
				airline: "",
				cost: "",
			},
		});
	};
	render() {
		const { classes } = this.props;
		return (
			<div>
				<h1>Add Flights</h1>
				<div>
					<FormControl
						variant="outlined"
						style={{ minWidth: "250px" }}
						className={classes.formControl}
					>
						<InputLabel id="demo-simple-select-outlined-label">
							Airline
						</InputLabel>
						<Select
							labelId="demo-simple-select-outlined-label"
							id="demo-simple-select-outlined"
							label="Airline"
							name="airline"
							value={this.state.flightDetails.airline}
							onChange={this.onChangeHandler}
						>
							{this.props.allAirline.map((item, index) => {
								return <MenuItem value={item.code}>{item.name}</MenuItem>;
							})}
						</Select>
					</FormControl>
					<br />
					<br />
					<TextField
						id="standard-basic"
						label="Seats"
						type="text"
						name="seats"
						value={this.state.flightDetails.seats}
						variant="outlined"
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<FormControl
						variant="outlined"
						className={classes.formControl}
						style={{ minWidth: "250px" }}
					>
						<InputLabel id="demo-simple-select-outlined-label">From</InputLabel>
						<Select
							labelId="demo-simple-select-outlined-label"
							id="demo-simple-select-outlined"
							label="From"
							name="from"
							value={this.state.flightDetails.from}
							onChange={this.onChangeHandler}
						>
							{this.props.allCities.map((item, index) => {
								return <MenuItem value={item.code}>{item.name}</MenuItem>;
							})}
						</Select>
					</FormControl>
					<br />
					<br />
					<FormControl
						variant="outlined"
						className={classes.formControl}
						style={{ minWidth: "250px" }}
					>
						<InputLabel id="demo-simple-select-outlined-label">To</InputLabel>
						<Select
							value={this.state.flightDetails.to}
							labelId="demo-simple-select-outlined-label"
							id="demo-simple-select-outlined"
							label="To"
							name="to"
							onChange={this.onChangeHandler}
						>
							{this.props.allCities.map((item, index) => {
								return (
									<MenuItem key={index} value={item.code}>
										{item.name}
									</MenuItem>
								);
							})}
						</Select>
					</FormControl>
					<br />
					<br />
					<TextField
						labelId="demo-simple-select-outlined-label"
						id="demo-simple-select-outlined"
						label="Departure Time"
						variant="filled"
						name="departureTime"
						variant="outlined"
						value={this.state.flightDetails.departureTime}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<TextField
						id="filled-basic"
						label="Arrival Time"
						variant="filled"
						name="arrivalTime"
						variant="outlined"
						value={this.state.flightDetails.arrivalTime}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<TextField
						labelId="demo-simple-select-outlined-label"
						id="demo-simple-select-outlined"
						label="Flight Number"
						variant="filled"
						name="flightNumber"
						variant="outlined"
						value={this.state.flightDetails.flightNumber}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<TextField
						labelId="demo-simple-select-outlined-label"
						id="demo-simple-select-outlined"
						label="Cost"
						variant="filled"
						name="cost"
						variant="outlined"
						value={this.state.flightDetails.cost}
						onChange={this.onChangeHandler}
					/>
					<br />
					<br />
					<Button
						variant="contained"
						color="primary"
						onClick={this.onFlightSubmit}
					>
						submit
					</Button>
				</div>
			</div>
		);
	}
}
const mapStateToProps = (state) => {
	console.log(state);
	return {
		allAirline: state.airlines,
		allCities: state.cities,
	};
};

export default withStyles(useStyles)(
	withRouter(connect(mapStateToProps)(withRouter(Flights)))
);
