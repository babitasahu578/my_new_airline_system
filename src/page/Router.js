import React from "react";
import { Switch, Route } from "react-router-dom";
// import Home from "../Home";
import Airlines from "./Airlines";
import Home from "./Home";
import Flights from "./Flights";
import City from "./City";
import Booking from "./Booking";

export default function Router() {
	return (
		<div>
			<Home />
			<Switch>
				<Route exact path="/airline" component={Airlines}></Route>
				<Route exact path="/flights" component={Flights}></Route>
				<Route exact path="/city" component={City}></Route>
				<Route exact path="/booking" component={Booking}></Route>
			</Switch>
		</div>
	);
}
