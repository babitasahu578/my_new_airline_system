import React from "react";
import logo from "./logo.svg";
import "./App.css";
import Router from "./page/Router";
// import Router from './page/routes/Router'

function App() {
	return (
		<div className="App">
			<Router />
		</div>
	);
}

export default App;
