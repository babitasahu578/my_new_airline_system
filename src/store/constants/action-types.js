export const ADD_AIRLINE = "ADD_AIRLINE";
export const ADD_FLIGHT = "ADD_FLIGHT";
export const ADD_CITY = "ADD_CITY";
export const UPDATE_FLIGHT = "UPDATE_FLIGHT";
export const SEARCH_FLIGHT = "SEARCH_FLIGHT";
export const BOOKED_FLIGHT = "BOOKED_FLIGHT";
export const RESET = "RESET";
