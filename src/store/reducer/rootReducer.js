import {
	ADD_AIRLINE,
	ADD_CITY,
	ADD_FLIGHT,
	SEARCH_FLIGHT,
	BOOKED_FLIGHT,
	UPDATE_FLIGHT,
	RESET,
} from "../constants/action-types";
const initialState = {
	airlines: [],
	flights: [],
	cities: [],
	searchedFlights: [],
	bookedFlight: "",
	data: "",
};

const rootReducer = (state = initialState, action) => {
	console.log(action);

	switch (action.type) {
		case ADD_AIRLINE: {
			return {
				...state,
				airlines: [...state.airlines, action.payload],
			};
		}
		case ADD_FLIGHT: {
			return {
				...state,
				flights: [...state.flights, action.payload],
			};
		}
		case ADD_CITY: {
			return {
				...state,
				cities: [...state.cities, action.payload],
			};
		}
		case SEARCH_FLIGHT: {
			console.log(state);

			const data = state.flights.filter(
				(item) =>
					item.from === action.payload.fromKeyword &&
					item.to === action.payload.toKeyword
			);
			return {
				...state,
				searchedFlights: data,
			};
		}
		case BOOKED_FLIGHT: {
			console.log(action.payload);

			return {
				...state,
				flights: state.flights.map((item) => {
					if (item.flightNumber == action.payload.flightNumber) {
						let newData = {
							...action.payload,
							seats:
								Number(action.payload.seats) - Number(action.payload.people),
						};
						return newData;
					}
					return item;
				}),

				bookedFlight: action.payload,
			};
		}
		case RESET: {
			return {
				...state,
				searchedFlights: [...initialState.searchedFlights],
			};
		}

		default:
			return state;
	}
};
export default rootReducer;
