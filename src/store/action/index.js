import {
	ADD_AIRLINE,
	ADD_CITY,
	ADD_FLIGHT,
	SEARCH_FLIGHT,
	BOOKED_FLIGHT,
	UPDATE_FLIGHT,
	RESET,
} from "../constants/action-types";

export function addAirline(payload) {
	return { type: ADD_AIRLINE, payload: payload };
}
export function reset() {
	return { type: RESET };
}

export function addFlight(payload) {
	return { type: ADD_FLIGHT, payload: payload };
}

export function addCity(payload) {
	return { type: ADD_CITY, payload: payload };
}
export function searchFlight(payload) {
	return { type: SEARCH_FLIGHT, payload: payload };
}
export function bookedFlight(payload) {
	return { type: BOOKED_FLIGHT, payload: payload };
}

export function updateFlight(payload, id) {
	return { type: UPDATE_FLIGHT, payload: payload, id: id };
}
